<?php

// Blank message to start with so we can append to it.
$message = '';

// Construct the message

$name = '';
$name = $_POST['first'] . " " . $_POST['last'];

$interests = join(", ",$_POST['interests']);


$message .= <<<TEXT
    Name: {$name}
    Email: {$_POST['email']}
    Interests: {$interests}
    Affiliation: {$_POST['affiliation']}
    
    {$_POST['comments']}
TEXT;

// Email to send to
$to = 'snyderome-helpdesk@lists.stanford.edu';

//$reply = 'edwong57@stanford.edu';

// Email Subject
$subject = "iPOP contact form: " . $_POST['subj'];

// Name to show email from
//$from =  $name;

// Domain to show the email from
//$fromEmail = '';

// Construct a header to send who the email is from
$header = 'From: ' . $name . '<' . $_POST['email'] . '>'; //"\r\n".

// Try sending the email
if(!mail($to, $subject, $message, $header)){
    die('There has been an error sending your email.');
} else {
    die('Thank you for contacting us. You\'re email has sent!');
}

?>